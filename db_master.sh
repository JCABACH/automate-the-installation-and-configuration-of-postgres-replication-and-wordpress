#!/bin/bash
# Установка  бд postgress
sudo apt-get update
sudo apt-get install -y postgresql

# создание пользователя для репликации

sudo -i -u postgres psql -c "ALTER SYSTEM SET listen_addresses TO '*';"
sudo -i -u postgres createuser --replication -e repluser
sudo -i -u postgres psql -c "ALTER USER repluser PASSWORD '1234';"
sudo -i -u postgres psql -c "create database wordpress;"
sudo -i -u postgres psql -c "CREATE USER wpuser with PASSWORD '1234';"
sudo -i -u postgres psql -c "grant all privileges on database wordpress to wpuser;"

# редактирование файла pg_hba.conf

sudo sed -i -e '$a host    replication     repluser        192.168.100.10/24       md5' /etc/postgresql/12/main/pg_hba.conf
sudo sed -i -e '$a host    replication     repluser        192.168.100.20/24       md5' /etc/postgresql/12/main/pg_hba.conf
sudo sed -i '/# IPv4 local connections:/ a \host    all             wpuser          192.168.100.30/24       md5' /etc/postgresql/12/main/pg_hba.conf

# редактирование файла postgresql.conf

sudo sed -i '/#listen_addresses = '\''localhost'\''/c\listen_addresses = '\''localhost, 192.168.100.10'\''' /etc/postgresql/12/main/postgresql.conf
sudo sed -i '/#wal_level = replica/c\wal_level = replica' /etc/postgresql/12/main/postgresql.conf
sudo sed -i '/#max_wal_senders = 10/c\max_wal_senders = 2' /etc/postgresql/12/main/postgresql.conf
sudo sed -i '/#max_replication_slots = 10/c\max_replication_slots = 2' /etc/postgresql/12/main/postgresql.conf
sudo sed -i '/#hot_standby = on/c\hot_standby = on' /etc/postgresql/12/main/postgresql.conf
sudo sed -i '/#hot_standby_feedback = off/c\hot_standby_feedback = on' /etc/postgresql/12/main/postgresql.conf

#Перезапуск бд
sudo systemctl restart postgresql
